<!DOCTYPE html>
<html>
<head>
	<title>Admin Login Form</title>
	<link rel="stylesheet" type="text/css" href="Connexion.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script type="https://kit.fontawesome.com/a81368914c.js"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>


	<meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>

<?php
?>
<div class="container md">
		<div class="img">
			<img src="Images/admin.svg">
		</div>
		<div class="login-container">
			<form method=POST>
				<img class="avatar" src="Images/avatar1.svg">
				<h2>Welcome</h2>
				<div class="input-div one">
					<div class="i">
						<i class="fas fa-user"></i>						
					</div>
					<div>
						<!--h5>Username</h5-->
						<input type="text" name="username" placeholder="  Username" class="input" required>
					</div>
				</div>
				<div class="input-div two">
					<div class="i">
						<i class="fas fa-lock"></i>						
					</div>
					<div class="div">
						<input type="password" name="password" placeholder="  Password"class="input" required>
					</div>
				</div>
				<a href="HomeAdmin.php">	<input name='login'type="submit" class="btn" value="Login">	</a>
				
            </form>	
            <?php
                if(isset($_POST['login']))
                {
                    $conn =new PDO("mysql:host=localhost;port=3306;dbname=location","root","", array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

                    $uname=null;
                    $pass=null;
					$uname = $_POST['username'];
					$pass = $_POST['password'];
					
					$query = "SELECT * FROM administrateur WHERE username = '$uname' AND pwd = '$pass'";
					$rs = $conn->query($query);
					$num = $rs->num_rows;
					$rows = $rs->fetch(PDO::FETCH_ASSOC);
					if($rows > 0){
						session_start();
						$_SESSION['username'] = $rows['username'];
						$_SESSION['password'] = $rows['password'];
						echo "<script type = \"text/javascript\">
									alert(\"Login Successful.................\");
									window.location = (\"HomeAdmin.php\")
									</script>";
					} else{
						echo "<script type = \"text/javascript\">
									alert(\"Login Failed. Try Again................\");
									window.location = (\"AdminLogin.php\")
									</script>";
					}
				}
			?>
			</div>
	
</body>