<!DOCTYPE html>
<html>
<head>
	<title>Adding Car Form</title>
	<link rel="stylesheet" type="text/css" href="Inscription.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script type="https://kit.fontawesome.com/a81368914c.js"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
     integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
	<div class="container md">
		<div class="img">
			<img src="Images/car.svg">
		</div>
		<div class="login-container">
            <!--enctype pour charger l'image-->
			<form method="POST" action = "AjouterVoitureTraiter.php" enctype="multipart/form-data" >
				<img class="avatar" src="Images/avatar1.svg">
				<h2>Add Car</h2>
				<div class="input-div one">
					<div class="i">
                        <i class="fas fa-car"></i>						
					</div>
					<div>
						<input name="immatricul" type="text" placeholder="   Immatriculation:DK-3969-AA" class="input" required>
					</div>
				</div>
				<div class="input-div two">
					<div class="i">
                        <i class="fas fa-car"></i>					
                     </div>
					<div class="div">
						<input name="prix" type="int" placeholder="   Example: 70000frcs" class="input" required>
					</div>
                </div>
                <div class="input-div tree">
					<div class="i">
                        <i class="fas fa-car"></i>						
					</div>
					<div class="div">
						<input name="capacite" type="number" placeholder="    Example: 6 places"  maxlength="9" class="input" required>
					</div>
                </div>
            
               
                <div class="input-div four">
					<div class="i">
                        <i class="fas fa-car"></i>
                	</div>
					<div>
						<input name="model" type="text" placeholder="   Example:4x4" class="input"  required>
					</div>
				</div>
                <div class="input-div five">
					<div class="i">
                    <i class="fas fa-car"></i>           
                    </div>
					<div>
						<input name="marque" type="text" placeholder="     Example: Peugeot" class="input"  required>
					</div>
                </div>
                <div class="input-div six">

					<div>
						<input name="fich" type="file"  maxlength="13" class="input"  required>
					</div>
                </div>
                <p>
                <div id="statut"> <label id='action' for="statut">Statut:  </label>
			        <input name="statut" type="radio"  value='0'/> Available
                    <input name="statut" type="radio"  value='1'/> Not Available
                </div>
                </p>
				<!--a href="#">Forgot Password?</a-->
				<input type="submit" class="btn" value="Save"></a>	
			</form>	
		</div>
	</div>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>