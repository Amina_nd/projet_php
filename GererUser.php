<?php include("Session.php");?>
<!DOCTYPE html>
<html>
<head>
	<title>Managing User</title>
	
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Home </title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
	
    <!--  Bootstrap css file  -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!--  font awesome icons  -->
    <link rel="stylesheet" href="css/all.min.css">


    <!--  Magnific Popup css file  -->
    <link rel="stylesheet" href="vendor/Magnific-Popup/dist/magnific-popup.css">


    <!--  Owl-carousel css file  -->
    <link rel="stylesheet" href="vendor/owl-carousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl-carousel/css/owl.theme.default.min.css">


    <!--  custom css file  -->
    <link rel="stylesheet" href="GererUser.css">

    <!--  Responsive css file  -->
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body>
	 <!--  ======================= Start Header Area ============================== -->


<header class="header_area">
        <div class="main-menu">
            <nav class="navbar navbar-expand-lg navbar-light">
            <!--a class="nav-link" href="#"><span class="sr-only">Queens Location Admin</span></li></a-->

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <div class="mr-auto"></div>
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="AjouterVoiture.php">Add Car <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="HomeAdmin.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">History</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Users</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Hire</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Accueil.php">Logout</a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </header>

 <!--  ======================= End Header Area ============================== -->

  <!--  ======================= Start Card Area ============================== >
 	 <section class="card-area">
  			<div class="container carousel py-lg-5">
                <div class="owl-carousel owl-theme">
                    <div class="client row">
                        <div class="col-lg-4 col-md-12 client-img">
                            <img src="./img/testimonials/t1.jpg" alt="img1" class="img-fluid">
                        </div>
                        <div class="col-lg-8 col-md-12 about-client">
                            <h4 class="text-uppercase">John Martin</h4>
                            <p class="para">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quidem architecto
                                consequuntur ratione, obcaecati corrupti deserunt.</p>
                        </div>
                    </div>
                    <div class="client row">
                        <div class="col-lg-4 col-md-12 client-img">
                            <img src="./img/testimonials/t2.jpg" alt="img2" class="img-fluid">
                        </div>
                        <div class="col-lg-8 col-md-12 about-client">
                            <h4 class="text-uppercase">Mac Hill</h4>
                            <p class="para">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quidem architecto
                                consequuntur ratione, obcaecati corrupti deserunt.</p>
                        </div>
                    </div>
                    <div class="client row">
                        <div class="col-lg-4 col-md-12 client-img">
                            <img src="./img/testimonials/t1.jpg" alt="img1" class="img-fluid">
                        </div>
                        <div class="col-lg-8 col-md-12 about-client">
                            <h4 class="text-uppercase">John Martin</h4>
                            <p class="para">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quidem architecto
                                consequuntur ratione, obcaecati corrupti deserunt.</p>
                        </div>
                    </div>
                    <div class="client row">
                        <div class="col-lg-4 col-md-12 client-img">
                            <img src="./img/testimonials/t2.jpg" alt="img2" class="img-fluid">
                        </div>
                        <div class="col-lg-8 col-md-12 about-client">
                            <h4 class="text-uppercase">Mac Hill</h4>
                            <p class="para">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quidem architecto
                                consequuntur ratione, obcaecati corrupti deserunt.</p>
                        </div>
                    </div>
                    <div class="client row">
                        <div class="col-lg-4 col-md-12 client-img">
                            <img src="./img/testimonials/t1.jpg" alt="img1" class="img-fluid">
                        </div>
                        <div class="col-lg-8 col-md-12 about-client">
                            <h4 class="text-uppercase">John Martin</h4>
                            <p class="para">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quidem architecto
                                consequuntur ratione, obcaecati corrupti deserunt.</p>
                        </div>
                    </div>
                    <div class="client row">
                        <div class="col-lg-4 col-md-12 client-img">
                            <img src="./img/testimonials/t2.jpg" alt="img2" class="img-fluid">
                        </div>
                        <div class="col-lg-8 col-md-12 about-client">
                            <h4 class="text-uppercase">Mac Hill</h4>
                            <p class="para">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quidem architecto
                                consequuntur ratione, obcaecati corrupti deserunt.</p>
                        </div>
                    </div>
                </div>
            </div>
    </section-->
    

    <section class="users-area">
        <div class="container" style="text-align:center; display:inline; margin:12px; padding:12px;">
        <table class="table table-hover">
            <thead class="thead-light" style="text-align:center; margin:2px;">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Username</th>
                    <th scope="col">Birthday</th>
                    <th scope="col">Password</th>
                    <th scope="col">Address</th>
                    <th scope="col">Mail</th>
                    <th scope="col">Cni</th>
                    <th scope="col">Number</th>
                    <th scope="col">Gender</th>
                    <th scope="col">Control</th>
                </tr>
            </thead>
             <?php
                $baseD = new PDO("mysql:host=localhost;port=3306;dbname=location", "root", "", array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                //include 'config.php';
                $sel = "SELECT * FROM user";
                $result = $baseD->query($sel);
                while($row = $result->fetch(PDO::FETCH_ASSOC)){	
            ?>
          <!--echo $ligne['id'].' | '.$ligne['username'].' | ' .$ligne['birthday'].' | '.$ligne['pwd'].' | '.$ligne['adress'].' | '.$ligne['mail'].' | '.$ligne['cni'].' | '.$ligne['phone']. ' | '.$ligne['gender'];  -->         
                <tr>
                 <td scope="row" style="text-align:center;"><?php echo $row['id_user'] ?></td>
                    <td scope="row"><?php echo $row['username'] ?></td>
                    <td scope="row"><?php echo $row['birthday'] ?></td>
                    <td scope="row"><?php echo $row['pwd'] ?></td>
                    <td scope="row"><?php echo $row['adress'] ?></td>
                    <td scope="row"><?php echo $row['mail'] ?></td>
                    <td scope="row"><?php echo $row['cni'] ?></td>
                    <td scope="row"><?php echo $row['phone'] ?></td>
                    <td scope="row"><?php echo $row['gender'] ?></td>
                    <td scope="row"><a href="javascript:sureToApprove(<?php echo $row['id'];?>)" class="ico del"><i class="fas fa-trash"></i></a><a href="#" class="ico edit"><i class="fas fa-edit"></i></a></td>
                </tr>
            <?php
                 }    
            ?>
        </table>
    </div>
    </section>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>