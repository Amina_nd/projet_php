<!DOCTYPE html>
<html>
<head>
	<title>Signin Form</title>
	<link rel="stylesheet" type="text/css" href="Inscription.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script type="https://kit.fontawesome.com/a81368914c.js"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>


	<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
	<div class="container md">
		<div class="img">
			<img src="Images/signin.svg">
		</div>
		<div class="login-container">
			<form method="POST" action = "InscriptionTraiter.php">
				<img class="avatar" src="Images/avatar1.svg">
				<h2>Welcome</h2>
				<div class="input-div one">
					<div class="i">
						<i class="fas fa-user"></i>						
					</div>
					<div>
						<input name="username" type="text" placeholder="   Username" class="input" required>
					</div>
				</div>
				<div class="input-div two">
					<div class="i">
                    <i class="fas fa-at"></i>					
                </div>
					<div class="div">
						<input name="mail" type="email" placeholder="   becomeuser@queenslocation.com" class="input" required>
					</div>
                </div>
                <div class="input-div tree">
					<div class="i">
						<i class="fas fa-phone"></i>						
					</div>
					<div class="div">
						<input name="phone" type="tel" placeholder="    Phone Number"  maxlength="9" class="input" required>
					</div>
                </div>
                <div class="input-div four">
					<div class="i">
                        <i class="fas fa-birthday-cake"></i>						
					</div>
					<div class="div">
						<input name="birthday" type="date"  placeholder="   Birthday" class="input" required>
					</div>
                </div>
               
                <div class="input-div five">
					<div class="i">
						<i class="fas fa-lock"></i>						
					</div>
					<div>
						<input name="password" type="password" placeholder="   Password" class="input"  required>
					</div>
				</div>
                <div class="input-div six">
					<div class="i">
                         <i class="fas fa-map-marker-alt"></i>           
                    </div>
					<div>
						<input name="address" type="text" placeholder="   Location" class="input"  required>
					</div>
                </div>
                <div class="input-div seven">
					<div class="i">
                        <i class="fas fa-address-card"></i>
					</div>
					<div>
						<input name="cni" type="text" placeholder="   CNI : carte d'identité nationale" maxlength="13" class="input"  required>
					</div>
                </div>
                <p>
                <div id="sexe"> <label id='action' for="Sexe">Sexe:  </label>
			        <input name="gender" type="radio"  value='M'/> M
                    <input name="gender" type="radio"  value='F'/> F
                </div>
                </p>
				<!--a href="#">Forgot Password?</a-->
				<a href="InscriptionTraiter.php"><input type="submit" class="btn" value="Subscribe"></a>	
			</form>	
		</div>
	</div>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>