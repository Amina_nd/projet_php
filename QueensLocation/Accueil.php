<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Queens Location</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

	<meta charset="utf-8">
	<meta name="author" content="pixelhint.com">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
	
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="Accueil.css">

	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    

 <!--  Bootstrap css file  -->
 <link rel="stylesheet" href="css/bootstrap.min.css">

<!--  font awesome icons  -->
<link rel="stylesheet" href="css/all.min.css">



<!--  Magnific Popup css file  -->
<link rel="stylesheet" href="vendor/Magnific-Popup/dist/magnific-popup.css">


<!--  Owl-carousel css file  -->
<link rel="stylesheet" href="vendor/owl-carousel/css/owl.carousel.min.css">
<link rel="stylesheet" href="vendor/owl-carousel/css/owl.theme.default.min.css">


<!--  custom css file  -->
<link rel="stylesheet" href="Accueil.css">
<link rel="stylesheet" type="text/css" href="css/reset.css">


<!--  Responsive css file  -->
<link rel="stylesheet" href="css/responsive.css">

<!--  Jquery js file  -->
<script src="js/jquery.3.4.1.js"></script>

<!--  Bootstrap js file  -->
<script src="js/bootstrap.min.js"></script>

<!--  isotope js library  -->
<script src="vendor/isotope/isotope.min.js"></script>

<!--  Magnific popup script file  -->
<script src="vendor/Magnific-Popup/dist/jquery.magnific-popup.min.js"></script>

<!--  Owl-carousel js file  -->
<script src="vendor/owl-carousel/js/owl.carousel.min.js"></script>

<!--  custom js file  -->
<script src="js/main.js"></script>


</head>
<body>
<!--Navigation-->
<header class="header_area">
        <div class="main-menu">
            <nav class="navbar navbar-expand-lg navbar-light">
            <!--a class="nav-link" href="#"><span class="sr-only">Queens Location Admin</span></li></a-->

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <div class="mr-auto"></div>
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Team</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Accueil.php">Connect</a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </header>


<!--Image Slider-->
<div id="slides" class="carousel slide" data-ride="carousel">
		<ul class="carousel-indicators">
			<li data-target="#slides" data-slide-to="0" class="active"></li>
			<li data-target="#slides" data-slide-to="1"></li>
			<li data-target="#slides" data-slide-to="2"></li>
		</ul>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img src="Images/font.png">
				<div class="carousel-caption">
					<h1 class="display-4">Queens Location</h1>
					<h3>Autorent</h3>
					<a href="Connexion.php"><button type="button" class="btn btn-light btn-lg">Se Connecter</button></a>	
                    <a href="Inscription.php"><button type="button" class="btn btn-primary btn-lg">S'inscrire</button></a>
				</div>
			</div>
			<div class="carousel-item">
				<img src="Images/font1.jpg">
				<div class="carousel-caption">
					<h1 class="display-4">Queens Location</h1>
					<h3>Autorent</h3>
					<a href="Connexion.php"><button type="button" class="btn btn-light btn-lg">Se Connecter</button></a>	
                    <a href="Inscription.php"><button type="button" class="btn btn-primary btn-lg">S'inscrire</button></a>
				</div>
			</div>
			<div class="carousel-item">
				<img src="Images/font2.jpg">
				<div class="carousel-caption">
					<h1 class="display-4">Queens Location</h1>
					<h3>Autorent</h3>
					<a href="Connexion.php"><button type="button" class="btn btn-light btn-lg">Se Connecter</button></a>	
                    <a href="Inscription.php"><button type="button" class="btn btn-primary btn-lg">S'inscrire</button></a>
				</div>
			</div>

		</div>
		
</div>




<hr class="my-4">


<!--- Discover us -->
<!--div class="container-fluid padding"-->
<section class="">
	<section id="caption ">
		<div class="row welcome text-center">
			
				<div class="col-12">
					<p class="display-4" style="text-align: center; font-weight:600; font-size:36px;">Find You Dream Cars For Hire</h2>
					<p class="properties display-6 bold" style="text-align: center">Range Rovers - Mercedes Benz - Landcruisers</h3>
				</div>
				<hr>
			
		</div>
	</section>
	</section>
	
	<section class="listings">
		<div class="wrapper">
			<ul class="properties_list">
			<?php
				$baseD =new PDO("mysql:host=localhost;port=8889;dbname=location","root","root", array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
				$sel = "SELECT * FROM vehicule WHERE statut = '0'";
				$rs = $baseD->query($sel);
				while($rws = $rs->fetch(PDO::FETCH_ASSOC)){
			?>
				<li>
					<a href="VehiculeDetails.php?id=<?php echo $rws['idV'] ?>">
						<img class="thumb" src="Images/<?php echo $rws['imag'];?>" width="300" height="200">
					</a>
					</br>
					<span class="price"><?php echo $rws['prix'].'F CFA';?></span>
					<div class="property_details">
						<p class="display-6">
							<a href="VehiculeDetails.php?id=<?php echo $rws['idV'] ?>"><?php echo 'Car Make>'.$rws['model'];?></a><br>
							<h8>Car Marque/Model: <span class="property_size"><?php echo $rws['marque'];?></span></h8>
						</p>
					</div>
				</li>
			<?php
				}
			?>
			</ul>
		</div>
	</section>	<!--  end listing section  -->


	
<!--/div-->



    <hr class="my-4">
	


    <!--- Footer -->
<footer style="background-color:#f8f9fa; heigth:25%;">
		<!--Rejoignez-nous en vous incrivant-->

		<section id="social-media" >
			<p class= "display-4" style="color:black;">FIND US ON SOCIAL MEDIA</p>
			<div class="social-icons">
				<a href="#"><img src="Images/facebook2.png"></a>
				<a href="#"><img src="Images/linkdlin.jpeg"></a>
				<a href="#"><img src="Images/twitter.jpeg"></a>
				<a href="#"><img src="Images/instagram.jpeg"></a>
				<a href="#"><img src="Images/snap.jpeg"></a>
				<a href="#"><img src="Images/whtspp.jpeg"></a>    
    		</div>
		</section>
</footer>


</body>  
</html>
