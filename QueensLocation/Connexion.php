<?php session_start();?>
<!DOCTYPE html>
<html>
<head>
	<title>Login Form</title>
	<link rel="stylesheet" type="text/css" href="Connexion.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script type="https://kit.fontawesome.com/a81368914c.js"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>


	<meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>


	<div class="container md">
		<div class="img">
			<img src="Images/login.svg">
		</div>
		<div class="login-container">
			<form method=POST action="ConnexionTraiter.php">
				<img class="avatar" src="Images/avatar1.svg">
				<h2>Welcome</h2>
				<div class="input-div one">
					<div class="i">
						<i class="fas fa-user"></i>						
					</div>
					<div>
						<!--h5>Username</h5-->
						<input type="text" name="username" placeholder="  Username" class="input" required>
					</div>
				</div>
				<div class="input-div two">
					<div class="i">
						<i class="fas fa-lock"></i>						
					</div>
					<div class="div">
						<input type="password" name="password" placeholder="  Password"class="input" required>
					</div>
				</div>
				<a href="#">Forgot Password?</a>
				<!--<a href="Home.php">-->	<input type="submit" class="btn" value="Login">	</a>
				
				<?php
                if(isset($_GET['erreur'])){
                    $err = $_GET['erreur'];
                    if($err==1 || $err==2)
                        echo "<p style='color:red'>Username or password not correct</p>";
                }
                ?>
			</form>	
		</div>
	</div>
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/jquery.3.4.1.js">
</body>
</html>