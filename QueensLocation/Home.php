<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome to Queens Location </title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
	
    <!--  Bootstrap css file  -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!--  font awesome icons  -->
    <link rel="stylesheet" href="css/all.min.css">


    <!--  Magnific Popup css file  -->
    <link rel="stylesheet" href="vendor/Magnific-Popup/dist/magnific-popup.css">


    <!--  Owl-carousel css file  -->
    <link rel="stylesheet" href="vendor/owl-carousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl-carousel/css/owl.theme.default.min.css">


    <!--  custom css file  -->
    <link rel="stylesheet" href="Home.css">

    <!--  Responsive css file  -->
    <link rel="stylesheet" href="css/responsive.css">
     <!--  Jquery js file  -->
     <script src="js/jquery.3.4.1.js"></script>

<!--  Bootstrap js file  -->
<script src="js/bootstrap.min.js"></script>

<!--  isotope js library  -->
<script src="vendor/isotope/isotope.min.js"></script>

<!--  Magnific popup script file  -->
<script src="vendor/Magnific-Popup/dist/jquery.magnific-popup.min.js"></script>

<!--  Owl-carousel js file  -->
<script src="vendor/owl-carousel/js/owl.carousel.min.js"></script>

<!--  custom js file  -->
<script src="js/main.js"></script>

	</head>
	<body>
		
<header class="header_area">
        <div class="main-menu">
            <nav class="navbar navbar-expand-lg navbar-light">
            <!--a class="nav-link" href="#"><span class="sr-only">Queens Location Admin</span></li></a-->

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <div class="mr-auto"></div>
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Favorites</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Accueil.php">Logout</a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
	</header>
	

	<?php
			$conn =new PDO("mysql:host=localhost;port=8889;dbname=location","root","root", array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
			$select = "SELECT * FROM vehicule WHERE statut = '0' ";
			$result = $conn->query($select);
			while($row = $result->fetch(PDO::FETCH_ASSOC)){
	?>
	<div class="card mb-3" style="max-width: 540px;">
		<div class="row no-gutters">
			<div class="col-md-4">
				<img src="Images/<?php echo $row['imag'];?> " class="card-img" width='200' heigth='200' >
			</div>
			<div class="col-md-8">
				<div class="card-body">
					<h5 class="card-title">Vehicle model:<?php echo $row['model'] ?></h5>
					<p class="card-text">Vehicle mark: <?php echo $row['marque'] ?></p>
					<p class="card-text"><medium class="text-muted">Vehicle price:<?php echo $row['prix'].' Frcs CFA' ?></medium></p>
    				<p class="card-text"><small class="text-muted">Vehicle capacity:<?php echo $row['capicite'].' Places' ?></small></p>
					<a href="Reservation.php" class="btn btn-primary" style="border:1px solid gray; width:30%;">Hire</a>
				</div>
			</div>
		</div>
		</div>
		<?php
			}					
		?>

<footer class="footer-area">
        <div class="container">
            <div class="">
                <div class="social text-center">
                    <h5 class="text-uppercase">Follow us</h5>
                    <a href="#"><i class="fab fa-facebook"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-youtube"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                </div>
                <div class="copyrights text-center">
                    <p class="para">
                        Copyright ©2020 All rights reserved | This work is made with by
                        <a href="#"><span style="color: var(--primary-color);">Queens</span></a>
                    </p>
                </div>
            </div>
        </div>
</footer>
	
		<!--  Jquery js file  -->
		<script src="js/jquery.3.4.1.js"></script>

		<!--  Bootstrap js file  -->
		<script src="js/bootstrap.min.js"></script>

		<!--  isotope js library  -->
		<script src="vendor/isotope/isotope.min.js"></script>

		<!--  Magnific popup script file  -->
		<script src="vendor/Magnific-Popup/dist/jquery.magnific-popup.min.js"></script>

		<!--  Owl-carousel js file  -->
		<script src="vendor/owl-carousel/js/owl.carousel.min.js"></script>

		<!--  custom js file  -->
		<script src="js/main.js"></script>

	</body>
</html>