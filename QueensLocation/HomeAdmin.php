<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Home </title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
	
    <!--  Bootstrap css file  -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!--  font awesome icons  -->
    <link rel="stylesheet" href="css/all.min.css">


    <!--  Magnific Popup css file  -->
    <link rel="stylesheet" href="vendor/Magnific-Popup/dist/magnific-popup.css">


    <!--  Owl-carousel css file  -->
    <link rel="stylesheet" href="vendor/owl-carousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl-carousel/css/owl.theme.default.min.css">


    <!--  custom css file  -->
    <link rel="stylesheet" href="HomeCss.css">

    <!--  Responsive css file  -->
    <link rel="stylesheet" href="css/responsive.css">
     <!--  Jquery js file  -->
     <script src="js/jquery.3.4.1.js"></script>

<!--  Bootstrap js file  -->
<script src="js/bootstrap.min.js"></script>

<!--  isotope js library  -->
<script src="vendor/isotope/isotope.min.js"></script>

<!--  Magnific popup script file  -->
<script src="vendor/Magnific-Popup/dist/jquery.magnific-popup.min.js"></script>

<!--  Owl-carousel js file  -->
<script src="vendor/owl-carousel/js/owl.carousel.min.js"></script>

<!--  custom js file  -->
<script src="js/main.js"></script>
<script type="text/javascript">
    function sureToApprove($id){
        if(confirm("Are you sure you want to delete this car?")){
            window.location.href ='deletevehicle.php?id='+$id;
        }
    }
</script>


</head>

<body>
 <!--  ======================= Start Header Area ============================== -->


<header class="header_area">
        <div class="main-menu">
            <nav class="navbar navbar-expand-lg navbar-light">
            <!--a class="nav-link" href="#"><span class="sr-only">Queens Location Admin</span></li></a-->

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <div class="mr-auto"></div>
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="AjouterVoiture.php">Add Car <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="HomeAdmin.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="GererUser.php">Users</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Hire</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Accueil.php">Logout</a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </header>

 <!--  ======================= End Header Area ============================== -->

<!--  ======================= Start Main Area ================================ -->
<main class="site-main">


<!--  ======================= Start Banner Area =======================  -->
<section class="site-banner">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-6 col-md-12 site-title">
                <h3 class="title-text">Hey</h3>
                <h1 class="title-text text-uppercase">I am OTA</h1>
                <h4 class="title-text text-uppercase">Queens Autorent's Administrator</h4>
                
            </div>
            <div class="col-lg-6 col-md-12 banner-image">
                <img src="banner/banner-image.png" alt="banner-img" class="img-fluid">
            </div>
            
        </div>
    </div>
</section>
<!--  ======================= End Banner Area =======================  -->

<!--  ======================= Start Voiture Area =======================  -->
<section class="site-box">
<div class="container">
           <!-- Box Head -->
            <h4 class="col-md-6 display-6" style="display:inline; text-align:center;"> All Vehicles:
							<label>Research vehicles</label>
							<input type="text" class="field small-field" style="align:center;"/>
							<input type="submit" class="button" value="search" style="width:15%; marging-left:10%; background-color:light;" /></h4>
                            
				
					
				
						<table class="table table-hover" width="90%">
                        <thead class="thead-light">
							<tr>
								<th scope="col">Vehicle model</th>
                                <th scope="col">Vehicle marque</th>
                                <th scope="col">Vehicle immatriculation</th>
                                <th scope="col">Vehicle image</th>
                                <th scope="col">Vehicle Capacity</th>
                                <th scope="col">Vehicle Status</th>
								<th scope="col">Price</th>
								<th scope="col">Content Control</th>
                            </tr>
                        </thead>
							<?php
                                $conn =new PDO("mysql:host=localhost;port=8889;dbname=location","root","root", array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                                $select = "SELECT * FROM vehicule WHERE statut = '0' ";
								$result = $conn->query($select);
								while($row = $result->fetch(PDO::FETCH_ASSOC)){
							?>
							<tr>
								<td scope="row"><h5><?php echo $row['model'] ?></h5></td>
                                <td><?php echo $row['marque'] ?></td>
                                <td><?php echo $row['immatricul'] ?></td>
                                <td><img class="thumb" src="Images/<?php echo $row['imag'];?>" width="150" height="100"></td>
                                <td><?php echo $row['capicite'].'  Places' ?></td>
                                <td><?php echo $row['statut'] ?></td>
								<td><?php echo $row['prix'].' FrcsCFA' ?></td>
								<td><a href="javascript:sureToApprove(<?php echo $row['idV'];?>)" class="ico del" style="margin-left:20%; color:red;"><i class="fas fa-trash"></i></a><?php echo "   ";?><a href="#" class="ico edit" style="margin-left:20%;"><i class="fas fa-edit"></i></a></td>
							</tr>
							<?php
								}
							?>
						</table>
				
						
</div>		
	
</section>
   
				<!-- End Box -->


<!--  ======================= End Voiture Area =======================  -->

</main>
 <!--  ======================= End Main Area ================================ -->

 <footer class="footer-area">
        <div class="container">
            <div class="">
                <!--div class="site-logo text-center py-4">
                    <a href="#"><img src="Images/QueensAuTORENT.gif" alt="logo" class="anime"></a>
                </div-->
                <div class="social text-center">
                    <h5 class="text-uppercase">Follow us</h5>
                    <a href="#"><i class="fab fa-facebook"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-youtube"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                </div>
                <div class="copyrights text-center">
                    <p class="para">
                        Copyright ©2020 All rights reserved | This work is made with by
                        <a href="#"><span style="color: var(--primary-color);">Queens</span></a>
                    </p>
                </div>
            </div>
        </div>
    </footer>


    <!--  Jquery js file  -->
    <script src="js/jquery.3.4.1.js"></script>

    <!--  Bootstrap js file  -->
    <script src="js/bootstrap.min.js"></script>

    <!--  isotope js library  -->
    <script src="vendor/isotope/isotope.min.js"></script>

    <!--  Magnific popup script file  -->
    <script src="vendor/Magnific-Popup/dist/jquery.magnific-popup.min.js"></script>

    <!--  Owl-carousel js file  -->
    <script src="vendor/owl-carousel/js/owl.carousel.min.js"></script>

    <!--  custom js file  -->
    <script src="js/main.js"></script>





</body>
</html>