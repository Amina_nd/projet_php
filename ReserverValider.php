<?php include("ReserverTraiter.php");?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hire a car</title>

    <link rel="stylesheet" type="text/css" href="reserver.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script type="https://kit.fontawesome.com/a81368914c.js"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
     integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
    <!--  Bootstrap css file  -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!--  font awesome icons  -->
    <link rel="stylesheet" href="css/all.min.css">


    <!--  Magnific Popup css file  -->
    <link rel="stylesheet" href="vendor/Magnific-Popup/dist/magnific-popup.css">


    <!--  Owl-carousel css file  -->
    <link rel="stylesheet" href="vendor/owl-carousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl-carousel/css/owl.theme.default.min.css">


    <!--  custom css file  -->
    <link rel="stylesheet" href="reserver.css">

    <!--  Responsive css file  -->
    <link rel="stylesheet" href="css/responsive.css">
     <!--  Jquery js file  -->
     <script src="js/jquery.3.4.1.js"></script>

<!--  Bootstrap js file  -->
<script src="js/bootstrap.min.js"></script>

<!--  isotope js library  -->
<script src="vendor/isotope/isotope.min.js"></script>

<!--  Magnific popup script file  -->
<script src="vendor/Magnific-Popup/dist/jquery.magnific-popup.min.js"></script>

<!--  Owl-carousel js file  -->
<script src="vendor/owl-carousel/js/owl.carousel.min.js"></script>

<!--  custom js file  -->
<script src="js/main.js"></script>



</head>

<body>
 <!--  ======================= Start Header Area ============================== -->
<header class="header_area">
        <div class="main-menu">
            <nav class="navbar navbar-expand-lg navbar-light">
            <!--a class="nav-link" href="#"><span class="sr-only">Queens Location Admin</span></li></a-->

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <div class="mr-auto"></div>
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="Home.php">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Favorites</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Profile</a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="Accueil.php">Logout</a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </header>

    <!--?php
        $conn =new PDO("mysql:host=localhost;port=3306;dbname=location","root","", array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        $select = "SELECT * FROM reservation WHERE idR = '1' ";
		$result = $conn->query($select);
	    while($row = $result->fetch(PDO::FETCH_ASSOC)){
    ?-->

    <!--img src="Images/" alt="">
    <div class="card border-light mb-3" style="max-width: 18rem;float:right;">
        <div class="card-header"><--?php echo 'Validate Your Rent'?></div>
        <div class="card-body">
            <h5 class="card-title"><--?php echo 'Username' ?></h5>
            <p class="card-text">Date of departure: <--?php echo $row['Datedebut'] ?></p>
            <p class="card-text">Return date: <--?php echo $row['Datefin'] ?></p>
            <p class="card-text">Place of departure: <--?php echo $row['LieuDep'] ?></p>
            <p class="card-text">Return place: <--?php echo $row['LieuRet'] ?></p>



        </div>
    </div-->
    <!--?php
        }
    ?-->

</body>
</html>