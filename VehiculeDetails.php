<!DOCTYPE html>
<html lang="en">
<head>
	<title>Vehicule Details</title>
	<meta charset="utf-8">
	<meta name="author" content="pixelhint.com">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
	
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
 <!--  Bootstrap css file  -->
 <link rel="stylesheet" href="css/bootstrap.min.css">

<!--  font awesome icons  -->
<link rel="stylesheet" href="css/all.min.css">



<!--  Magnific Popup css file  -->
<link rel="stylesheet" href="vendor/Magnific-Popup/dist/magnific-popup.css">


<!--  Owl-carousel css file  -->
<link rel="stylesheet" href="vendor/owl-carousel/css/owl.carousel.min.css">
<link rel="stylesheet" href="vendor/owl-carousel/css/owl.theme.default.min.css">


<!--  custom css file  -->
<link rel="stylesheet" href="Vehicule.css">
<link rel="stylesheet" type="text/css" href="css/reset.css">


<!--  Responsive css file  -->
<link rel="stylesheet" href="css/responsive.css">

<!--  Jquery js file  -->
<script src="js/jquery.3.4.1.js"></script>

<!--  Bootstrap js file  -->
<script src="js/bootstrap.min.js"></script>

<!--  isotope js library  -->
<script src="vendor/isotope/isotope.min.js"></script>

<!--  Magnific popup script file  -->
<script src="vendor/Magnific-Popup/dist/jquery.magnific-popup.min.js"></script>

<!--  Owl-carousel js file  -->
<script src="vendor/owl-carousel/js/owl.carousel.min.js"></script>

<!--  custom js file  -->
<script src="js/main.js"></script>


</head>
<body>
	<!--Navigation-->
<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
		<div class="container-fluid">
			
			<button class="navbar-toggler" type="button" data-toggler="collapse"
			 data-target="#navbarResponsive">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link" href="Accueil.php">Home</a>
					</li> 
					<li class="nav-item">
						<a class="nav-link" href="Accueil.php">About</a>
					</li> 
					<li class="nav-item">
						<a class="nav-link" href="Accueil.php">Service</a>
					</li> 
					<li class="nav-item">
						<a class="nav-link" href="Accueil.php">Team</a>
					</li> 
					<li class="nav-item">
						<a class="nav-link" href="Accueil.php">Connect</a>
					</li> 
				</ul>
				
			</div>
		</div>

</nav>


    

	<section class="">
			<section class="caption">
				<h2 class="caption" style="text-align: center">Find You Dream Cars For Rent</h2>
				<h3 class="properties" style="text-align: center">Range Rovers - Mercedes Benz - Landcruisers</h3>
			</section>
	</section><!--  end hero section  -->
	
	<section class="listings">
		<div class="wrapper">
			<ul class="properties_list">
			<?php
                $baseD =new PDO("mysql:host=localhost;port=3306;dbname=location","root","", array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                 $sel = "SELECT * FROM vehicule WHERE idV = '$_GET[id]' ";
				$rs = $baseD->query($sel);
				$rws = $rs->fetch(PDO::FETCH_ASSOC);
			?>
				<li>
					<a href="VehiculeDetails.php?id=<?php echo $rws['idV'] ?>">
						<img class="thumb" src="Images/<?php echo $rws['imag'];?>" width="300" height="200">
					</a>
					<span class="price"><?php echo $rws['prix'] .' F CFA';?></span>
					<div class="property_details">
						<h1>
							<a href="VehiculeDetails.php?id=<?php echo $rws['idV'] ?>"><?php echo 'Car Make>'.$rws['model'];?></a>
						</h1>
						<h2>Car Name/Model: <span class="property_size"><?php echo $rws['marque'];?></span></h2>
					</div>
				</li>
				<h3> 
				<div class="alert alert-danger" role="alert"  style='float:rigth; margin-left:20%;'>
					Veuillez vous inscrire ou vous connecter pour reserver cette voiture <?php echo $rws['marque'];?>
				</div>
				</h3>
				
				<a href="Connexion.php"><button type="button" class="btn btn-light btn-lg">Se Connecter</button></a>	
                    <a href="Inscription.php"><button type="button" class="btn btn-primary btn-lg">S'inscrire</button></a>
				
				
			</ul>
		</div>
	</section>	<!--  end listing section  -->

	
</body>
</html>